# mydirectory-api-sample

This is an example showing the usage of the myDirectory API:
* A login as an enterprise administrator
* Getting some information on contacts (Users, Groups, Services)
* Getting the list of users and their forwardings
* This script was written to help developer understand the use of myDirectory API

# Pre-requisites
* Use python3 (3.8 recommended)
* Import useful modules 
# Quick start

Clone the repository:

    git clone git@gitlab.com:centile-pub/myrcc-api-sample

# Launch the script

    python3.8 myDirectory.py

# More info

Please refer to our PartnerConnect website (https://partnerconnect.centile.com) or contact me.
