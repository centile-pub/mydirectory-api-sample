# Importing some useful libraries

import requests, json, base64, re, socket, websocket, random, time, sys, pprint
from datetime import datetime


# Reading data from data.json file entered as an argument

url = "https://mytelephony.<your-domain-com>/restletrouter"
message = "<enterprise_login>:<enterprise_pass>"

# Encoding base64 the login/password

message_bytes = message.encode()
base64_bytes = base64.b64encode(message_bytes)
base64_message = base64_bytes.decode()
type(base64_message)
rd = random.random()

# Defining the initial header

payload = {
    'X-Application': 'myDirectory',
    'Content-Type': 'application/json',
    'Accept': 'application/json, text/plain, */*'}
payload['Authorization'] = "Basic " + base64_message


# Login phase

login = requests.post(url + "/v1/service/Login", headers=payload)

if str(login.status_code) == "401":
    print("====================================")
    print(f"Check your credentials (Error {login.status_code})")
    print("====================================")
    sys.exit()

if str(login.status_code) == "200":
    print("\n")
    print("==============================================================================")
    print(f"Login successful ({login.status_code})")
    print("==============================================================================")

else:
    print("Login failed. Check your credentials or the server availability! ")

r_dict = login.headers

# Getting X-Application and Set-Cookie headers, beautifying them

xapp = r_dict['X-Application']
cookie = r_dict['Set-Cookie'].split('myDirectory_SESSIONID')[-1].split(';')[0]
cookie = "myDirectory_SESSIONID" + cookie

payload_auth = {
    'Content-Type': 'application/json',
    'Accept': 'application/json, text/plain, */*'}
payload_auth['X-Application'] = xapp
payload_auth['Cookie'] = cookie

# Getting the list of contacts of the enterprise (Users, groups, services)

get_contacts = requests.get(url + "/v1/public/contacts", headers=payload_auth)
print(get_contacts.text)
ct_list = json.loads(get_contacts.text)

print("==============================================================================")
print("|- Name - | - Type - | - Ext - | - Status - | - PSTN - | - PLMN - | \n")
for x in range(0,int(ct_list['count'])):
    print(str(ct_list['items'][x]['displayName']) + " - " \
        + str(ct_list['items'][x]['alias']) + " - " \
        + str(ct_list['items'][x]['publicInfo']['addressNumber']) + " - " \
            + str(ct_list['items'][x]['publicInfo']['telephonicState']) + " - " \
                + str(ct_list['items'][x]['publicInfo']['pstnNumbers']) + " -" \
                    + str(ct_list['items'][x]['publicInfo']['plmnNumbers']) + " - " \
                        + "\n"
                    )

# Getting the list of forwarding rules (not very usable this way, commenting this)
#get_fwd = requests.get(url + "/v1/public/Forwarding", headers=payload_auth)
#pprint.pprint(get_fwd.text)
#fwd_list = json.loads(get_fwd.text)
#print("--------------------")

#for x in range(0, int(fwd_list['count'])):
    #print(str(fwd_list['items'][x]))
    # print(f"Name: {fwd_list['items'][x]['alias']}")
    # print(f"Type: {fwd_list['items'][x]['forwardType']}")
    # if "destination" in fwd_list['items'][x]:
    #     print(f"Destination: {fwd_list['items'][x]['destination']}")
    # print(f"Filter: {fwd_list['items'][x]['filter']}")
    # print("--------------------")

# More details on users (with their forwarding rules)

print("==============================================================================")
print("|- Name - | - Ext - | - List of Active Forwardings")
print("==============================================================================")

for x in range(0,int(ct_list['count'])):
    if ct_list['items'][x]['alias'] == 'IUser':
        get_user = requests.get(url + "/v1/public/contacts/" + str(ct_list['items'][x]['contactId']) + "/Forwarding", headers=payload_auth)
        user_details = json.loads(get_user.text)

        print(str(ct_list['items'][x]['displayName']) + " - " \
            + str(ct_list['items'][x]['publicInfo']['addressNumber']) + " - ")
        print("===================")

        for x in range(0,user_details['count']):
            if user_details['items'][x]['activated'] == True:
                print(f"Destination: {user_details['items'][x]['destination']}")
                print(f"Filter: {user_details['items'][x]['filter']}")
                print(f"Label: {user_details['items'][x]['label']}")
                print("--------------------")   
        print("\n===================")

# Logout

logout = requests.get(url + "/v1/service/Logout", headers=payload)
logout_status = str(logout.status_code)

print("==============================================================================")
if logout_status == "204":
    
    print(f"End of the operations - Logout OK ({logout_status}) - ")
else:
    print(f"End of the operations - Error during logout ({logout_status})")
print("==============================================================================")
